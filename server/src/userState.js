import User from './model/user.js'
import Room from './model/room.js'

export default class UserState {
  userId
  currentRoomId

  /**
   * @return {boolean}
   */
  isLoggedIn() {
    return this.userId !== undefined
  }

  /**
   * @return {boolean}
   */
  isInRoom() {
    return this.userId !== undefined && this.currentRoomId !== undefined
  }

  /**
   * @return {Promise<User>}
   */
  async getUser() {
    if (!this.isLoggedIn()) {
      console.log('Getting user when user not logged in')
      throw new Error('User not logged in')
    }

    try {
      const user = await User.findById(this.userId)
      if (user) {
        return user
      }
    } catch (err) {
      console.log('Error getting user', {
        error: err,
        userId: this.userId,
      })
      throw new Error('Could not load user: ' + err)
    }

    throw new Error('User not found')
  }

  /**
   * @return {Promise<Room>}
   */
  async getRoom() {
    if (!this.isInRoom()) {
      console.log('Getting room when user is not in one', {
        userId: this.userId,
        roomId: this.currentRoomId,
      })
      throw new Error('User is not in room')
    }

    try {
      const room = await Room.findById(this.currentRoomId)
      if (room) {
        return room
      }
    } catch (err) {
      console.log('Error getting room', {
        error: err,
        userId: this.userId,
        roomId: this.currentRoomId,
      })
      throw new Error('Could not load room: ' + err)
    }

    throw new Error('Room not found')
  }
}
