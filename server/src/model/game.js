import mongoose from 'mongoose'

const MINE = -1
const BOMB_RADIUS = 1

const CellSchema = new mongoose.Schema({
  value: { type: Number, enum: [-1, ...Array(9)] },
  revealed: { type: Boolean, default: false },
  revealedBy: String,
})

CellSchema.methods.reveal = function (user) {
  this.revealed = true
  this.revealedBy = user._id.toString()
}

export const GameSchema = new mongoose.Schema({
  board: [[CellSchema]],
  started: { type: Boolean, default: false },
  finished: { type: Boolean, default: false },
  mineCount: Number,
})

GameSchema.methods.start = function () {
  this.started = true
}

GameSchema.methods.reset = function () {
  this.board = createBoard(this.board.length, this.mineCount)
  this.started = false
  this.finished = false
}

/**
 * Reveals a cell (and neighboring if it has zero neighboring mines)
 * and returns the earned score.
 *
 * @param {number} x
 * @param {number} y
 * @param {User} user
 *
 * @return {number} Score earned by the user revealing
 */
GameSchema.methods.reveal = function (x, y, user) {
  if (!this.board[x] || !this.board[x][y]) {
    return 0
  }
  const cell = this.board[x][y]
  if (!cell.revealed) {
    cell.reveal(user)
    if (cell.value === MINE) {
      return 1
    }
    if (cell.value === 0) {
      for (let i = x - 1; i <= x + 1; i++) {
        for (let j = y - 1; j <= y + 1; j++) {
          if (i !== x || j !== y) {
            this.reveal(i, j, user)
          }
        }
      }
    }
  }
  return 0
}

/**
 * Places a bomb revealing the cell and neighboring ones with a set radius
 * and returns the earned score.
 *
 * @param {number} x
 * @param {number} y
 * @param {User} user
 *
 * @return {number} Score earned by the user revealing
 */
GameSchema.methods.bomb = function (x, y, user) {
  let score = 0
  for (let i = x - BOMB_RADIUS; i <= x + BOMB_RADIUS; i++) {
    for (let j = y - BOMB_RADIUS; j <= y + BOMB_RADIUS; j++) {
      score += this.reveal(i, j, user)
    }
  }
  return score
}

/**
 * @param {Object<string, User>} users
 * @return {({revealedBy, revealed, value}|{revealed: boolean})[][]}
 */
GameSchema.methods.publicBoard = function (users) {
  return this.board.map(row =>
    row.map(cell =>
      cell.revealed
        ? {
            value: cell.value,
            revealed: true,
            revealedBy: users[cell.revealedBy],
          }
        : { revealed: false }
    )
  )
}

/**
 * @param {number} boardSize
 * @param {number} mineCount
 * @returns {{value: number, revealed: boolean, revealedBy: ?string}[][]}
 */
export const createBoard = (boardSize, mineCount) => {
  const board = []
  for (let i = 0; i < boardSize; i++) {
    board[i] = []
    for (let j = 0; j < boardSize; j++) {
      board[i][j] = { value: 0 }
    }
  }

  let minesToPlace = mineCount
  while (minesToPlace > 0) {
    const coords = {
      x: Math.floor(Math.random() * boardSize),
      y: Math.floor(Math.random() * boardSize),
    }
    if (board[coords.x][coords.y].value !== MINE) {
      board[coords.x][coords.y].value = MINE
      --minesToPlace

      for (let i = coords.x - 1; i <= coords.x + 1; i++) {
        for (let j = coords.y - 1; j <= coords.y + 1; j++) {
          if (i >= 0 && j >= 0 && i < boardSize && j < boardSize) {
            const cell = board[i][j]
            if (cell.value !== MINE) {
              cell.value++
            }
          }
        }
      }
    }
  }

  return board
}
