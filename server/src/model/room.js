import User from './user.js'
import { createBoard, GameSchema } from './game.js'
import mongoose from 'mongoose'

const ROOM_SLUG_LENGTH = 6
const STARTING_BOMBS = 1

const RoomSchema = new mongoose.Schema({
  slug: String,
  ownerId: String,
  userIds: { type: [String], default: [] },
  scores: {
    type: Map,
    of: { type: Number, min: 0 },
    default: {},
  },
  bombs: { type: Map, of: { type: Number, min: 0 }, default: {} },
  currentIndex: { type: Number, min: 0, default: 0 },
  game: GameSchema,
  chat: [
    {
      from: String,
      text: String,
      sentAt: { type: Date, default: Date.now },
    },
  ],
  updatedAt: Date,
})

RoomSchema.pre('save', function () {
  this.set({ updatedAt: new Date() })
  this.markModified('updatedAt')
})

/**
 * @param {string} ownerId
 * @param {number} boardSize
 * @param {number} mineCount
 * @return {Room}
 */
RoomSchema.statics.createNew = function (ownerId, boardSize, mineCount) {
  return this.create({
    slug: generateSlug(),
    ownerId,
    game: {
      board: createBoard(boardSize, mineCount),
      mineCount,
    },
    scores: {},
    bombs: {},
  })
}

/**
 * @param {string} roomSlug
 * @return {Promise<Room>}
 */
RoomSchema.statics.findBySlug = function (roomSlug) {
  return this.findOne({ slug: { $eq: roomSlug } }).exec()
}

RoomSchema.statics.findByUserId = function (userId) {
  return this.find({ userIds: { $elemMatch: { $eq: userId } } })
    .sort({ updatedAt: -1 })
    .exec()
}

/**
 * @param {User} user
 */
RoomSchema.methods.join = function (user) {
  if (this.userIds.find(uid => uid === user._id.toString())) {
    return
  }

  this.userIds.push(user._id)

  if (!this.game.started) {
    this.scores.set(user._id, 0)
    this.bombs.set(user._id, STARTING_BOMBS)
  }
}

RoomSchema.methods.nextTurn = function () {
  this.currentIndex = (this.currentIndex + 1) % this.userIds.length
  while (!this.scores.has(this.userIds[this.currentIndex])) {
    this.currentIndex = (this.currentIndex + 1) % this.userIds.length
  }
}

/**
 * @return {Promise<Object>}
 */
RoomSchema.methods.export = async function () {
  const users = await User.find({ _id: { $in: this.userIds } }).exec()
  const usersDict = {}
  users.forEach(u => {
    usersDict[u._id] = u.export()
  })
  const pb = this.game.publicBoard(usersDict)
  const chat = this.chat.map(msg => {
    return {
      from: usersDict[msg.from],
      text: msg.text,
      sentAt: msg.sentAt,
    }
  })

  return {
    id: this.slug,
    ownerId: this.ownerId,
    users: this.userIds.map(uid => usersDict[uid]),
    scores: this.scores,
    bombs: this.bombs,
    currentIndex: this.currentIndex,
    game: {
      board: pb,
      started: this.game.started,
      finished: this.game.finished,
      mineCount: this.game.mineCount,
    },
    chat,
  }
}

/**
 * @return {string}
 */
RoomSchema.methods.currentUserId = function () {
  return this.userIds[this.currentIndex]
}

/**
 * @param {User} user
 * @param {number} scoreToAdd
 */
RoomSchema.methods.addScore = function (user, scoreToAdd) {
  this.scores.set(
    user._id,
    this.scores.has(user._id.toString())
      ? this.scores.get(user._id) + scoreToAdd
      : scoreToAdd
  )

  const ranking = Array.from(this.scores.values()).sort().reverse()
  const minesLeft = ranking.reduce(
    (acc, curr) => acc - curr,
    this.game.mineCount
  )

  if (this.scores.length <= 1) {
    if (minesLeft <= 0) {
      this.game.finished = true
    }
    return
  }

  if (minesLeft <= 0 || minesLeft + ranking[1] < ranking[0]) {
    this.game.finished = true
  }
}

RoomSchema.methods.startGame = function () {
  if (this.userIds.length > 1) {
    this.userIds = this.userIds.shuffled()
  }
  this.game.start()
}

RoomSchema.methods.resetGame = function () {
  this.scores = {}
  this.bombs = {}
  this.userIds.forEach(uid => {
    this.scores.set(uid, 0)
    this.bombs.set(uid, STARTING_BOMBS)
  })
  this.game.reset()
}

/**
 * @param {User} fromUser
 * @param {string} text
 * @return {Object}
 */
RoomSchema.methods.addChatMessage = function (fromUser, text) {
  const msg = { from: fromUser._id, text }
  this.chat.push(msg)
  return msg
}

const Room = mongoose.model('Room', RoomSchema)

export default Room

/**
 * @return {Array}
 */
Array.prototype.shuffled = function () {
  if (this.length <= 1) {
    return [...this]
  }

  const res = [...this]
  let current,
    top = res.length

  while (--top) {
    current = Math.floor(Math.random() * (top + 1))
    const tmp = res[current]
    res[current] = res[top]
    res[top] = tmp
  }

  return res
}

/**
 * @return {string}
 */
const generateSlug = () => {
  let result = ''
  const characters =
    'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < ROOM_SLUG_LENGTH; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}
