import {
  uniqueNamesGenerator,
  adjectives,
  animals,
} from 'unique-names-generator'
import mongoose from 'mongoose'

const UserSchema = new mongoose.Schema({
  name: String,
  color: String,
})

export const userColors = [
  '#f44336',
  '#e91e63',
  '#9c27b0',
  '#673ab7',
  '#3f51b5',
  '#2196f3',
  '#03a9f4',
  '#00bcd4',
  '#009688',
  '#4caf50',
  '#8bc34a',
  '#cddc39',
  '#ffeb3b',
  '#ffc107',
  '#ff9800',
  '#ff5722',
  '#795548',
  '#607d8b',
]

UserSchema.statics.createNew = function () {
  const name = generateName()
  const color = userColors[Math.floor(Math.random() * userColors.length)]

  return this.create({ name, color })
}

UserSchema.methods.export = function () {
  return {
    id: this._id,
    name: this.name,
    color: this.color,
  }
}

const User = mongoose.model('User', UserSchema)

const generateName = () => {
  return uniqueNamesGenerator({ dictionaries: [adjectives, animals] })
    .split('_')
    .map(w => w.slice(0, 1).toUpperCase() + w.slice(1))
    .join(' ')
}

export default User
