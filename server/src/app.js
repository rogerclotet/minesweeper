import express from 'express'
import http from 'http'
import { Server } from 'socket.io'
import routes from './routes.js'
import socket from './socket.js'
import mongoose from 'mongoose'

const app = express()
routes(app)

const httpServer = http.createServer(app)
const options =
  process.env.NODE_ENV === 'production'
    ? {}
    : {
        cors: {
          origin: 'http://localhost:3000',
          methods: ['GET'],
        },
      }
const io = new Server(httpServer, options)

const dbHost = process.env.DB_HOST
const dbName = process.env.DB_NAME
const dbUsername = process.env.DB_USERNAME
const dbPassword = process.env.DB_PASSWORD
const dbUrl = `mongodb://${dbUsername}:${dbPassword}@${dbHost}/${dbName}`

async function run() {
  await mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })

  socket(io)

  const port = parseInt(process.env.PORT) || 8000
  httpServer.listen(port)
}
run().catch(console.dir)
