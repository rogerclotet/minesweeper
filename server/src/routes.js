import express from 'express'

const routes = route => {
  const publicPath = process.env.PUBLIC_PATH
  if (publicPath) {
    route.use('/', express.static(publicPath))
    route.get('*', (req, res) => {
      res.sendFile(publicPath + '/index.html')
    })
  }
}

export default routes
