import logIn from './handlers/logIn.js'
import getRoomList from './handlers/getRoomList.js'
import createRoom from './handlers/createRoom.js'
import startGame from './handlers/startGame.js'
import newGame from './handlers/newGame.js'
import join from './handlers/join.js'
import reveal from './handlers/reveal.js'
import bomb from './handlers/bomb.js'
import editProfile from './handlers/editProfile.js'
import chatMessage from './handlers/chatMessage.js'
import UserState from './userState.js'
import idle from './handlers/idle.js'

/**
 * @param {Server} io
 */
export default io => {
  io.on('connection', socket => {
    const state = new UserState()
    const handle = eventHandler(socket, state)

    handle('log_in', logIn(socket, state))
    handle('get_room_list', getRoomList(socket, state))
    handle('create_room', createRoom(socket, state))
    handle('start_game', startGame(io, state))
    handle('new_game', newGame(io, state))
    handle('join', join(io, socket, state))
    handle('idle', idle(socket, state))
    handle('reveal', reveal(io, state))
    handle('bomb', bomb(io, state))
    handle('edit_profile', editProfile(io, socket, state))
    handle('chat_message', chatMessage(io, state))
  })
}

const eventHandler = (socket, state) => (event, fn) => {
  socket.on(event, async (...args) => {
    try {
      await fn(...args)
    } catch (err) {
      console.log('Error handling event', {
        event,
        err,
        userId: state.userId,
        roomId: state.currentRoomId,
      })
    }
  })
}
