import Room from '../model/room.js'

/**
 * @param {Socket} socket
 * @param {UserState} state
 * @return {function(): Promise<Room[]>}
 */
export default (socket, state) => async () => {
  const user = await state.getUser()

  const rooms = await Room.findByUserId(user._id)

  const exportedRooms = await Promise.all(
    rooms.map(async r => await r.export())
  )

  socket.emit('room_list', exportedRooms)
}
