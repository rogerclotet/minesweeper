import isRoomOwner from '../utils/isRoomOwner.js'
import broadcastRoom from '../utils/broadcastRoom.js'

/**
 * @param {Server} io
 * @param {UserState} state
 * @return {function(): Promise<void>}
 */
export default (io, state) => async () => {
  /** @type {[User, Room]} */
  const [user, room] = await Promise.all([state.getUser(), state.getRoom()])

  if (!isRoomOwner(user, room)) {
    return
  }

  room.resetGame()
  await room.save()

  await broadcastRoom(io, room)
}
