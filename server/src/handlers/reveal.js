import broadcastRoom from '../utils/broadcastRoom.js'
import handleCellsRevealed from '../utils/handleCellsRevealed.js'

/**
 * @param {Server} io
 * @param {UserState} state
 * @return {function([{x: number, y: number}]): Promise<void>}
 */
export default (io, state) => async ([coords]) => {
  /** @type {[User, Room]} */
  const [user, room] = await Promise.all([state.getUser(), state.getRoom()])

  if (
    room.currentUserId() !== user._id.toString() ||
    !room.game.started ||
    room.game.finished
  ) {
    console.log('Reveal cannot be performed by user', {
      userId: user._id,
      roomCurrentUserId: room.currentUserId(),
      gameStarted: room.game.started,
      gameFinished: room.game.finished,
    })
    return
  }

  const scoreToAdd = room.game.reveal(coords.x, coords.y, user)
  await handleCellsRevealed(scoreToAdd, user, room, io)

  await room.save()

  await broadcastRoom(io, room)
}
