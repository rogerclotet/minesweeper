import broadcastRoom from '../utils/broadcastRoom.js'

/**
 * @param {Server} io
 * @param {UserState} state
 * @return {function([string]): Promise<void>}
 */
export default (io, state) => async ([text]) => {
  /** @type {[User, Room]} */
  const [user, room] = await Promise.all([state.getUser(), state.getRoom()])

  room.addChatMessage(user, text)
  await room.save()

  await broadcastRoom(io, room)
}
