/**
 * @param {Socket} socket
 * @param {UserState} state
 * @return {function(): Promise<void>}
 */
export default (socket, state) => async () => {
  state.currentRoomId = undefined
}
