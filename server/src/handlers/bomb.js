import broadcastRoom from '../utils/broadcastRoom.js'
import handleCellsRevealed from '../utils/handleCellsRevealed.js'

/**
 * @param {Server} io
 * @param {UserState} state
 * @return {function([{x: number, y: number}]): Promise<void>}
 */
export default (io, state) => async ([coords]) => {
  /** @type {[User, Room]} */
  const [user, room] = await Promise.all([state.getUser(), state.getRoom()])

  if (
    room.currentUserId() !== user._id.toString() ||
    !room.game.started ||
    room.game.finished
  ) {
    console.log('Bomb cannot be placed by user', {
      userId: user._id,
      roomCurrentUserId: room.currentUserId(),
      gameStarted: room.game.started,
      gameFinished: room.game.finished,
    })
    return
  }

  if (!room.bombs.has(user._id.toString()) || room.bombs.get(user._id) <= 0) {
    console.log('Tried to place a bomb when not available', {
      userId: user._id,
      remainingBombs: room.bombs.get(user._id),
    })
    return
  }

  room.bombs.set(user._id, room.bombs.get(user._id) - 1)

  const scoreToAdd = room.game.bomb(coords.x, coords.y, user)
  await handleCellsRevealed(scoreToAdd, user, room, io)

  await room.save()

  await broadcastRoom(io, room)
}
