import User from '../model/user.js'
import Room from '../model/room.js'

/**
 * @param {Socket} socket
 * @param {UserState} state
 * @return {function([?number]): Promise<void>}
 */
export default (socket, state) => async ([lastId]) => {
  /** @type {User} */
  let user

  if (lastId) {
    try {
      user = await User.findById(lastId).exec()
      await subscribeToRooms(socket, user)
    } catch (err) {
      console.log('Error finding user in log in', { error: err, lastId })
      user = undefined
    }
  }

  if (!lastId || !user) {
    user = await createUser()
  }

  state.userId = user._id

  socket.join(user._id.toString())

  socket.emit('logged_in', user.export())
}

/**
 * @return {Promise<User|undefined>}
 */
const createUser = async () => {
  try {
    return await User.createNew()
  } catch (createErr) {
    console.log('Could not create user', { error: createErr })
    return Promise.resolve(undefined)
  }
}

const subscribeToRooms = async (socket, user) => {
  const rooms = await Room.findByUserId(user._id)
  rooms.forEach(room => {
    socket.join('room_' + room._id.toString())
  })
}
