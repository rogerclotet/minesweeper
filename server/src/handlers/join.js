import Room from '../model/room.js'
import broadcastRoom from '../utils/broadcastRoom.js'

/**
 * @param {Server} io
 * @param {Socket} socket
 * @param {UserState} state
 * @return {function([string]): Promise<void>}
 */
export default (io, socket, state) => async ([roomSlug]) => {
  const user = await state.getUser()
  /** @type {Room} */
  let room

  try {
    room = await Room.findBySlug(roomSlug)
    if (room === null) {
      socket.emit('room_not_found')
      state.currentRoomId = undefined
      return
    }
  } catch (err) {
    console.log('Could not join room', {
      error: err,
      userId: user._id,
      roomSlug,
    })
    socket.emit('room_not_found')
    state.currentRoomId = undefined
    throw err
  }

  await room.join(user)
  if (room.isModified()) {
    await room.save()
  }

  state.currentRoomId = room._id.toString()

  socket.join('room_' + state.currentRoomId)

  await broadcastRoom(io, room)
}
