import Room from '../model/room.js'

const MIN_BOARD_SIZE = 5
const MAX_BOARD_SIZE = 100
const MIN_MINE_COUNT = 5
const MAX_MINE_COUNT = 100

/**
 * @param {Socket} socket
 * @param {UserState} state
 * @return {function([number, number]): Promise<void>}
 */
export default (socket, state) => async ([boardSize, mineCount]) => {
  const user = await state.getUser()

  if (
    boardSize < MIN_BOARD_SIZE ||
    boardSize > MAX_BOARD_SIZE ||
    mineCount < MIN_MINE_COUNT ||
    mineCount > MAX_MINE_COUNT ||
    mineCount >= boardSize * boardSize
  ) {
    console.log('Tried to create room with wrong parameters', {
      userId: user._id,
      boardSize,
      mineCount,
    })
    return
  }

  const room = await Room.createNew(user._id, boardSize, mineCount)

  socket.emit('room_created', {
    id: room.slug,
  })
}
