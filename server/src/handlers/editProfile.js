import broadcastRoom from '../utils/broadcastRoom.js'
import { userColors } from '../model/user.js'

/**
 * @param {Server} io
 * @param {Socket} socket
 * @param {UserState} state
 * @return {function([{name: string}]): Promise<void>}
 */
export default (io, socket, state) => async ([{ name, color }]) => {
  validate(name, color)

  const user = await state.getUser()

  user.name = name.trim()
  user.color = color
  await user.save()

  socket.emit('logged_in', user.export())

  try {
    // TODO notify all rooms with the user
    const room = await state.getRoom()
    await broadcastRoom(io, room)
  } catch (err) {
    // Do nothing, user is not in a room
  }
}

const validate = (name, color) => {
  if (name.trim().length > 32) {
    throw new Error('Invalid length for name: ' + name.trim().length)
  }

  if (!userColors.includes(color)) {
    throw new Error('Invalid color: ' + color)
  }
}
