/**
 * @param {User} user
 * @param {Room} room
 * @return {boolean}
 */
export default async (user, room) => {
  return room.ownerId === user?.id
}
