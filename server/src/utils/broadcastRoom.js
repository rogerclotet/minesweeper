/**
 * @param {Server} io
 * @param {Room} room
 */
export default async (io, room) => {
  const roomData = await room.export()
  io.to('room_' + room._id).emit('room', roomData)
}
