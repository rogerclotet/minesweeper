export default async (scoreToAdd, user, room, io) => {
  if (scoreToAdd > 0) {
    room.addScore(user, scoreToAdd)
  } else {
    room.nextTurn()
    const currentUserId = room.currentUserId()
    if (currentUserId !== user._id.toString()) {
      const roomData = await room.export()
      io.to(currentUserId).emit('your_turn', roomData)
    }
  }
}
