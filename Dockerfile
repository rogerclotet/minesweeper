FROM node:15-alpine

WORKDIR /code
COPY server/package.json .
COPY server/yarn.lock .
RUN yarn install
COPY server .
