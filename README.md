# Minesweeper

## Server

```shell
docker-compose up -d
```

## Client

```shell
yarn install
yarn start
```
