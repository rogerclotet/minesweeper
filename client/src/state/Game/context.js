import { createContext } from 'react'

export const GameContext = createContext({ state: {}, actions: {} })
