#!/bin/sh

(
  sshpass -p $SSH_PASSWORD ssh $SSH_USERNAME@$SSH_IP -o StrictHostKeyChecking=no <<-EOF
    cd $SSH_PROJECT_FOLDER
    git pull
    cd $SSH_PROJECT_FOLDER/client
    yarn install
    yarn build
    cd $SSH_PROJECT_FOLDER
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml pull
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml build
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml down
    DB_NAME=$DB_NAME DB_USERNAME=$DB_USERNAME DB_PASSWORD=$DB_PASSWORD DB_ROOT_USERNAME=$DB_ROOT_USERNAME \
    DB_ROOT_PASSWORD=$DB_ROOT_PASSWORD DB_ADMIN_USERNAME=$DB_ADMIN_USERNAME DB_ADMIN_PASSWORD=$DB_ADMIN_PASSWORD \
      docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d --force-recreate
EOF
)
